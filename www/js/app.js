// Ionic Starter App
           
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ion-gallery', 'starter.controllers','starter.services','ngCordova','ngStorage','ngFileUpload','hm.readmore'])
//.constant('API_URL', 'http://localhost:3100/api/v1/')
.constant('DEV_MODE', false)
.constant('API_URL', 'http://api.salonsnearby.com/v1/')
.constant('BROWSER_MODE', true)
//.constant('SHA1','9D:F5:F3:FC:C3:B3:17:0B:79:2C:90:44:B3:B8:97:09:96:AA:FD:D3')
.constant('SHA1','0B:49:7B:3E:3B:CE:F8:7A:A2:B2:96:C2:61:A5:0F:DB:F9:11:C6:93')

.run(function($ionicPlatform,GeoLocationService,$rootScope,SessionService,Utility,$timeout,$interval,BROWSER_MODE) {  
  $ionicPlatform.ready(function() { 
    var counter=0;
    $timeout(function() 
    {
      if (navigator.splashscreen) 
      {
        navigator.splashscreen.hide();
      }
    }, 100);
    if (!BROWSER_MODE) 
    {
      GeoLocationService.getGeoLocation();  
    }
    
    function checkConnection() 
    {
      var networkState = navigator.connection.type;

      var states = {};
      states[Connection.UNKNOWN]  = 'Unknown connection';
      states[Connection.ETHERNET] = 'Ethernet connection';
      states[Connection.WIFI]     = 'WiFi connection';
      states[Connection.CELL_2G]  = 'Cell 2G connection';
      states[Connection.CELL_3G]  = 'Cell 3G connection';
      states[Connection.CELL_4G]  = 'Cell 4G connection';
      states[Connection.CELL]     = 'Cell generic connection';
      states[Connection.NONE]     = 'No network connection';
      if((counter==0) && (states[networkState]=='Unknown connection' || states[networkState]=='Cell 2G connection' || states[networkState]=='Cell generic connection') )
      {
        counter++;
        Utility.showToastMessage("Slow network, please wait..");
      }
      if (states[networkState]=='No network connection') 
      {
        Utility.showToastMessage("Connect to Internet");
      }
      //Utility.showToastMessage(states[networkState]);
      console.log('Connection type: ' + states[networkState]);
    }
    $interval(function()
    {
      if (!BROWSER_MODE) 
      {
        checkConnection();
      }      
    }, 3000)


    document.addEventListener("offline", onOffline, false);
    
    document.addEventListener("online", onLine, false);
    function onOffline() {
       // Handle the offline event
       //Utility.showToastMessage("Connect to Internet");
       /*alert('Connect to Internet');*/
    } 
    function onLine() {
       // Handle the offline event
       // Utility.showToastMessage("Connectted");
       /*alert('Connect to Internet');*/
       $rootScope.$broadcast('internet:connected'); 
    }    
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    var pushNotification;
    try 
    { 
        
      pushNotification = window.plugins.pushNotification;
      var today = new Date();
      if (device.platform == 'android' || device.platform == 'Android' ||
        device.platform == 'amazon-fireos' ) {
        pushNotification.register(successHandler, errorHandler, {"senderID":"377607405466","ecb":"onNotificationGCM"}); 
        // required!
      } else {
          pushNotification.register(tokenHandler, errorHandler, {"badge":"true","sound":"true","alert":"true","ecb":"onNotificationAPN"});  // required!
      }
    }
    catch(err) 
    { 
      txt="There was an error on this page.\n\n"; 
      txt+="Error description: " + err.message + "\n\n"; 
      //alert(txt); 
    }
        function tokenHandler (result) 
        {
          console.log("<"+result+">");
          
          SessionService.setDeviceToken(result);
          SessionService.setNotificationType("apns");  
              
          SessionService.updateToken();
            // Your iOS push server needs to know the token before it can push to this device
            // here is where you might want to send it the token for later use.
        }
        function successHandler (result) 
        {
          console.log("<"+result+">");
            // Your iOS push server needs to know the token before it can push to this device
            // here is where you might want to send it the token for later use.
        }
        function errorHandler (error) 
        {
          console.log("<"+JSON.stringify(error)+">");
          //alert("The error HERE is:"+error);
            // Your iOS push server needs to know the token before it can push to this device
            // here is where you might want to send it the token for later use.
        }

        onNotificationAPN = function (event) 
        {

          var notifiType = "apns";
          SessionService.setNotificationType(notifiType);
          // SessionService.setTransaction(event.transaction);
          // SessionService.setStudentId(event.passId);

          if (event.alert && event.foreground == 1)
          {
              navigator.notification.alert(event.alert);
              // NotificationService.processNotificationWhenAppLive();
          }

          if ( event.sound && event.foreground == 1)
          {
              var snd = new Media(event.sound);
              snd.play();
          }

          if ( event.badge )
          {
              pushNotification.setApplicationIconBadgeNumber(successHandler,errorHandler, event.badge);
          }
          if(event.foreground == 0)
          {
              // SessionService.setNotificationReceived(true);
              // NotificationService.processNotification();
          }
        }
        onNotificationGCM = function(e) 
        {
            switch( e.event )
            {
                                     
                case 'registered':
                    if ( e.regid.length > 0 )
                    {

    //                     console.log("Regid " + e.regid);
                        var tok = e.regid;
                        var notifiType = "gcm";
                        SessionService.setDeviceToken(tok);
                        SessionService.setNotificationType(notifiType);
                        SessionService.updateToken();
                    }
                break;
     
                case 'message':
              
                  // SessionService.setStudentId(e.payload.studentId);
                  // SessionService.setTransaction(e.payload.transaction);

                  // this is the actual push notification. its format depends on the data model from the push server
                  if(e.foreground == 0)
                    {

                        // SessionService.setNotificationReceived(true);
                        // NotificationService.processNotification();
                    }

                    else
                    {
                      //console.log(JSON.stringify(e));
                      navigator.notification.alert(e.payload.title + "\n"+ e.message);
                      //NotificationService.processNotificationWhenAppLive();
                    }
                break;
     
                case 'error':
                  alert('GCM error = '+e.msg);
                break;
     
                default:
                  alert('An unknown GCM event has occurred');
                  break;
            }
        }
  });
})

.config(function($stateProvider,$ionicConfigProvider, $urlRouterProvider,ionGalleryConfigProvider) {
  $ionicConfigProvider.backButton.previousTitleText(false);
  $ionicConfigProvider.backButton.icon('ion-android-arrow-back');
  $ionicConfigProvider.backButton.text(' ');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.login', {
    url: '/login/:view',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })

  .state('app.myProfile', {
    url: '/myProfile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })
  .state('app.contactUs', {
    url: '/contactUs',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact2.html',
        controller: 'ContactCtrl'
      }
    }
  })

  .state('app.buisness', {
    url: '/buisness',
    views: {
      'menuContent': {
        templateUrl: 'templates/buisness.html',
        controller: 'BuisnessCtrl'
      }
    }
  })
  .state('app.contact', {
    url: '/usContact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
        controller: 'ContactCtrl'
      }
    }
  })  
  .state('app.vender', {
    url: '/vender',
    views: {
      'menuContent': {
        templateUrl: 'templates/vender.html',
        //controller: 'ContactCtrl'
      }
    }
  })
  .state('app.work', {
    url: '/work',
    views: {
      'menuContent': {
        templateUrl: 'templates/work.html',
        //controller: 'ContactCtrl'
      }
    }
  })
  .state('app.advertise', {
    url: '/advertise',
    views: {
      'menuContent': {
        templateUrl: 'templates/advertise.html',
        //controller: 'ContactCtrl'
      }
    }
  })
  .state('app.addOffers', {
    url: '/addOffers',
    views: {
      'menuContent': {
        templateUrl: 'templates/add-offers.html',
        //controller: 'testCtrl'
      }
    }
  })
  .state('app.adBiz', {
    url: '/adBiz',
    views: {
      'menuContent': {
        templateUrl: 'templates/advertise-biz.html',
        //controller: 'testCtrl'
      }
    }
  })
  .state('app.updateInfo', {
    url: '/updateInfo',
    views: {
      'menuContent': {
        templateUrl: 'templates/update-info.html',
        //controller: 'testCtrl'
      }
    }
  })
  .state('app.otherQuery', {
    url: '/otherQuery',
    views: {
      'menuContent': {
        templateUrl: 'templates/other-query.html',
        //controller: 'testCtrl'
      }
    }
  })

  .state('app.privacy', {
    url: '/privacy',
    views: {
      'menuContent': {
        templateUrl: 'templates/privacy.html',
        //controller: 'testCtrl'
      }
    }
  })

  .state('app.tnc', {
    url: '/tnc',
    views: {
      'menuContent': {
        templateUrl: 'templates/tnc.html',
        //controller: 'testCtrl'
      }
    }
  })
  .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html',
        //controller: 'testCtrl'
      }
    }
  })


  .state('app.test', {
    url: '/test',
    views: {
      'menuContent': {
        templateUrl: 'templates/test.html',
        //controller: 'testCtrl'
      }
    }
  })

   .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainCtrl'
      }
    }
  })

  .state('app.listing', {
    url: '/listing/:city',
    views: {
      'menuContent': {
        templateUrl: 'templates/list-page.html',
        controller: 'ListingCtrl'
      }
    }
  })

   .state('app.offers', {
    url: '/offers',
    views: {
      'menuContent': {
        templateUrl: 'templates/offers.html',
        controller: 'OffersCtrl'
      }
    }
  })

    .state('app.salonDetail', {
    url: '/salonDetail/:id/:name/:view',
    views: {
      'menuContent': {
        templateUrl: 'templates/salon-detail.html',
        controller: 'SalonDetailCtrl'
      }
    }
  })

  .state('app.favorites', {
    url: '/favorites',
    views: {
      'menuContent': {
        templateUrl: 'templates/favorite-salons.html',
        controller: 'FavoritesCtrl'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })

    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

  .state('geoDetect', {
    url: '/geoDetect',
    templateUrl: 'templates/geo-detect.html',
    controller: 'GeoDetectCtrl'
  });
  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('/app/main');
  $urlRouterProvider.otherwise('/geoDetect');
  ionGalleryConfigProvider.setGalleryConfig({
            action_label: 'Close',
            toggle: false,
            row_size: 2,
            fixed_row_size: true
  });
});