angular.module('starter.services', [])

.factory('Utility', function($ionicLoading,$http) {
  return{
    showToastMessage: function(message)
    {
      $ionicLoading.show({template: message, noBackdrop: true, duration: 1000 });
    },
    getRandomStringWithLength: function(length)
    {
      var chars = '0123456789abcdefghijklmnopqrstuvwxyz';
      length = length || 6;
      var string = '', rnd;
      while (length > 0) 
      {
        rnd = Math.floor(Math.random() * chars.length);
        string += chars.charAt(rnd);
        length--;
      }
      return string;
    },
    getRandomNumberWithLength: function(length)
    {
      var chars = '123456789';
      length = length || 6;
      var string = '', rnd;
      while (length > 0) 
      {
        rnd = Math.floor(Math.random() * chars.length);
        string += chars.charAt(rnd);
        length--;
      }
      return string;
    },
    sendSms: function(mobile,message)
    {
      var SMS_URL = "http://sms.ssdindia.com/api/sendhttp.php?authkey=11864ApvmIjRh9oY56f9117b&mobiles=MOBILE_NUMBER&message=MESSAGE_TO_SEND&sender=EDUCNT&route=4";
      message = encodeURI(message);
      SMS_URL = SMS_URL.replace("MOBILE_NUMBER",mobile).replace("MESSAGE_TO_SEND",message);
      $http.get(SMS_URL);
       //$http.post(SMS_URL);
    }
  };
})

.factory('GeoLocationService', function($rootScope, SessionService,DEV_MODE)
{ 
      var gpsPromptShown = false; 
      var gpsPromptShown = false; 
      var WGS84_a = 6378137.0; 
      var WGS84_b = 6356752.3; 
      
      function deg2rad(degrees)
      { 
        var result1 = Math.PI*degrees/180.0; 
        return result1; 
      } 
      function rad2deg(radians) 
      { 
        return 180.0*radians/Math.PI;  
      } 

      function WGS84EarthRadius(lat) 
      { 
        An = WGS84_a*WGS84_a * Math.cos(lat); 
        Bn = WGS84_b*WGS84_b * Math.sin(lat); 
        Ad = WGS84_a * Math.cos(lat); 
        Bd = WGS84_b * Math.sin(lat); 
        return Math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) ); 
      } 
      return{ 
      getGeoLocation: function()
      { 
        var lat, lng;
        if (device) 
        {
          console.log("Device found:----"+JSON.stringify(device));
          if (device.platform == 'android' || device.platform == 'Android' || device.platform == 'amazon-fireos' )
          {
            console.log("device is :----"+device.platform);
            LocationPlugin.getLastLocation("Hello This is toast.",onSuccess,onError);
          }
          else
          {
            console.log("device is not android and it is :----"+device.platform);
            navigator.geolocation.getCurrentPosition(onSuccess, onError, {timeout:2000});
          }
        }
        function onSuccess(data) 
        { 
          // alert(JSON.stringify(data));
          if(data.status == 0){
            lat = data.latitude;
            lng = data.longitude;
            var geocoder = new google.maps.Geocoder(); 

            var latlng = new google.maps.LatLng(lat, lng); 
            geocoder.geocode({'latLng': latlng}, function(results, status) 
            { 
              if (status == google.maps.GeocoderStatus.OK) 
              { 
                if (results[0]) 
                { 
                  // SessionService.setAddress(results[0]); 
                  var city=results[0].address_components[3].long_name; 
                  console.log(city) 
                  var address = results[0].formatted_address.split(","); 
                  var newAddress = ""; 
                  for(var i = address.length-4, j = 0; i < address.length-1;i++)
                  { 
                    if(address[i]) 
                    { 
                      newAddress += ((j==0)?"":",")+address[i]; 
                      j++; 
                    } 
                  } 
                  $rootScope.$broadcast('geoLocation:fetch:success', lat, lng,newAddress); 
                } 
                else 
                { 
                  $rootScope.$broadcast('geoLocation:fetch:failure'); 
                } 
              } 
              else 
              { 
                $rootScope.$broadcast('geoLocation:fetch:failure'); 
                //alert('Geocoder failed due to: ' + status); 
              } 
            }); 
          } else {
            $rootScope.$broadcast('geoLocation:fetch:failure'); 
          }
        }; 



        // navigator.geolocation.getCurrentPosition(onSuccess, onError, {timeout:2000}); 
        // function onSuccess(position) 
        // { 
        //   lat = position.coords.latitude; 
        //   lng = position.coords.longitude; 

        //   var geocoder = new google.maps.Geocoder(); 

        //   var latlng = new google.maps.LatLng(lat, lng); 
        //   geocoder.geocode({'latLng': latlng}, function(results, status) 
        //   { 
        //     if (status == google.maps.GeocoderStatus.OK) 
        //     { 
        //       if (results[0]) 
        //       { 
        //         // SessionService.setAddress(results[0]); 
        //         var city=results[0].address_components[3].long_name; 
        //         console.log(city) 
        //         var address = results[0].formatted_address.split(","); 
        //         var newAddress = ""; 
        //         for(var i = address.length-4, j = 0; i < address.length-1;i++)
        //         { 
        //           if(address[i]) 
        //           { 
        //             newAddress += ((j==0)?"":",")+address[i]; 
        //             j++; 
        //           } 
        //         } 
        //   //alert("success"); 
        //         $rootScope.$broadcast('geoLocation:fetch:success', lat, lng,newAddress); 
        //       } 
        //       else 
        //       { 
        //         $rootScope.$broadcast('geoLocation:fetch:failure'); 
        //       } 
        //     } 
        //     else 
        //     { 
        //       $rootScope.$broadcast('geoLocation:fetch:failure'); 
        //       //alert('Geocoder failed due to: ' + status); 
        //     } 
        //   }); 
        // }; 

        function onError(error) 
        {         
          $rootScope.$broadcast('geoLocation:fetch:failure'); 
          if(DEV_MODE){ 
        //Do nothing 
          }
          /*else
          { 
            if(device.platform == 'android' || device.platform == 'Android')
            { 
              // if(!gpsPromptShown || SessionService.getDialFlag()){ 
              if(!gpsPromptShown)
              { 
                navigator.notification.confirm( 
                'salonsnearby wants to access your location for best results.', 
                onPrompt, 
                'Location Access', 
                 ['Dont Allow', 'Settings']); 
                gpsPromptShown = true; 
        
              } 
            } 
          } */
        } 

        /*function onPrompt(buttonIndex)
        { 
          if(buttonIndex == 2)
          { 
            window.plugins.webintent.startActivity(
            { 
              action: 'android.settings.LOCATION_SOURCE_SETTINGS'
            }, 
            function(){}, 
            function()
            {
              alert('failed to open URL via Android Intent')
            }); 
          } 
        } */
      }
    } 
})

.factory('ImageService', function($http,SessionService, $rootScope, $ionicActionSheet, API_URL,Upload)
{
  var imageUrl;
  /*function getUrlPath(){
    var urlPath = DSP_URL + "/rest/" + FILE_SERVICE.name + "/" + FILE_SERVICE.container;
    return urlPath;
  }*/

  /*function uploadFileToUrl (file, fileName, uploadUrl, entity){
      var fd = new FormData();
      fd.append('files', file);
      $http.post(uploadUrl + "/"+ fileName + ".jpeg" + "?app_name="+ DSP_API_KEY, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      })
      .success(function(success){
        //alert("file uploaded");
       // console.log("ok fine");
        $rootScope.$broadcast('image:uploaded:successfully', entity);
        //console.log(success);
      })
      .error(function(){
        alert("Upload Failed");
      });
  }*/
  function openCamera(pictureSourceType)
  {
    navigator.camera.getPicture(onSuccess, onFail,
    {
      quality: 50,
      sourceType: pictureSourceType,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 400,
      targetHeight: 400,
      correctOrientation: true
    });
  }

  function onSuccess(imageData)
  {
    imageUrl = "data:image/jpeg;base64,"+imageData;
    $rootScope.$broadcast('image:captured', imageUrl);
    SessionService.setImageChanged();
  }

  function onFail(message) 
  {
    alert('Failed because: ' + message);
    console.log("fail");
  } 

  function dataURItoBlob(dataURI) 
  {    
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  }
  return {

    uploadImage : function(dataURI,filename) {
          var query = {
              filename: filename+".jpeg",
              type: "image/png"
          };
          
          $http.post(API_URL+'images', query)
          .success(function(result) 
          {            
            Upload.upload({
                url: result.url, //s3Url
                transformRequest: function(data, headersGetter) {
                    var headers = headersGetter();
                    delete headers.Authorization;
                    return data;
                },
                fields: result.fields, //credentials
                method: 'POST',
                file: dataURItoBlob(dataURI)
            })
            .progress(function(evt) 
            {
              console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total));
            })
            .success(function(data, status, headers, config) 
            {
              // file is uploaded successfully
              $rootScope.$broadcast('imageFile:uploaded',filename);            
            })
            .error(function() 
            {
              console.log('file ' + config.file.name + 'is uploaded successfully. Response: ' + data);
            })
            .error(function() 
            {
              alert("error");
            });
          })
          .error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
          });

      },
    deleteFile:function(entity,entityId)
    {
      var request={};
      request.container = FILE_SERVICE.container;
      request.file_path = "/"+entity+"_"+entityId+".jpeg";
      DreamFactory.api.firstidFiles.deleteFile(request).then(
      function(result){
        $rootScope.$broadcast('imageFile:delete:success');
      },
      function(reject) 
      {
        console.log(reject);
      })
    },
    /*uploadImage : function(entity,entityId,dataURL){
      var imageName = entity+"_"+entityId;
      var file = dataURItoBlob(dataURL);
      var uploadUrl = getUrlPath() +"/";
      uploadFileToUrl(file, imageName, uploadUrl, entity);
    },*/
    /*getImagePath : function(entity,entityId){
      var imageName = entity+"_"+entityId + ".jpeg";
      var imagePath = getUrlPath() + "/" + imageName + "?app_name="+DSP_API_KEY;
      return imagePath;
    },*/
    camera : function(showDelete)
    {
      var buttonsToShow = [
      {text: 'Take Photo'},
      {text: 'Choose Existing Photo'}
      ];
      if(showDelete)
      {
        buttonsToShow.push({text: 'Delete Photo'})
      } 
      var hideSheet = $ionicActionSheet.show({
      buttons: buttonsToShow ,
      cancelText: 'Cancel',
      cancel: function(){

      },
      buttonClicked: function(index, buttons)
      {
        if(index == 0)
        {
          openCamera(Camera.PictureSourceType.CAMERA);
        }
        else if(index == 1)
        {
          openCamera(Camera.PictureSourceType.SAVEDPHOTOALBUM);
        }
        else if(index == 2)
        {
          $rootScope.$broadcast('delete:image:requested');
        }
        return true;
      }
    });
    }
  };
})

.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork)
{
  //var networkState = navigator.connection.type;

    /*var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    alert('Connection type: ' + states[networkState]);*/
  return {
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();    
      } else {
        return navigator.onLine;        
      }
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();    
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function(){
        if(ionic.Platform.isWebView()){
 
          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            console.log("went online");
          });
 
          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("went offline");
          });
 
        }
        else {
 
          window.addEventListener("online", function(e) {
            console.log("went online");
          }, false);    
 
          window.addEventListener("offline", function(e) {
            console.log("went offline");
          }, false);  
        }       
    }
  }
})

.factory('OfferService',function($rootScope,API_URL,$http,SessionService )
{  
  return{ 
          getOffers:function(type)
          {
            var url=API_URL+"deals?lat="+SessionService.getLocation().latitude+"&lng="+SessionService.getLocation().longitude+"";
            if (type.name!='All deals') 
            {              
              if (type.type=='T') 
              {
                url += "&typeId="+type.id;
              }
              else
              {
                url +="&gender='"+type.type+"'";
              }
            }
            $http.get(url)
            .success(
            function(response)
            { 
              $rootScope.$broadcast('offers:fetched',response);              
            })
            .error(
            function(response)
            {              
              console.log(JSON.stringify(response));
            })    
          },
          getAdvertisements:function()
          {
            var url=API_URL+"advertisements";
            $http.get(url)
            .success(
            function(response)
            {
              $rootScope.$broadcast('advertisements:fetched',response);                  
            })
            .error(
            function(response)
            {
              console.log("Ads err");
              console.log(JSON.stringify(response));
            }); 
          }
    }
})

.factory('SalonService',function($rootScope,API_URL,$http,SessionService )
{
  return{
          getSalonDetail:function(salonId)
          {
            var url=API_URL+"salons/"+salonId+"?lat="+SessionService.getLocation().latitude+"&lng="+SessionService.getLocation().longitude+"";
            $http.get(url)
            .success(
            function(response)
            {
              $rootScope.$broadcast('salon:detail:fetched',response); 
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getReviews:function(salonId)
          {
            var url=API_URL+"salons/"+salonId+"/reviews";
            $http.get(url)
            .success(
            function(response)
            {
              $rootScope.$broadcast('reviews:fetched',response); 
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getReviewStars:function(response)
          {
            for (var i = 0; i < response.length; i++) 
            {
              response[i].fullStars=[];
              response[i].blankStars=[];
              response[i].halfStar=false;
              if (response[i].userRating) 
              {
                response[i].userRating=parseFloat(response[i].userRating);
                response[i].userRating=response[i].userRating.toFixed(1);
                var fullStars=[];
                var blankStars=[];
                var fullRatingFloat=response[i].userRating; 
                var fullStar=parseInt(response[i].userRating);
                var halfStar= fullRatingFloat-fullStar;
                var blankStar=5-fullStar;
                for (var a = 0; a < fullStar; a++) 
                {
                  var id=a;
                  fullStars.push(id);
                }
                if (halfStar>0) 
                {
                  response[i].halfStar=true;
                  blankStar=blankStar-1;              
                }            
                for (var b = 0; b <blankStar ; b++) 
                {
                  var id=b;
                  blankStars.push(id);
                }
                response[i].fullStars=fullStars;
                response[i].blankStars=blankStars;                              
              }
              else
              {
                var blankStars=[];
                for (var c = 0; c < 5; c++) 
                {
                  var id=c;
                  blankStars.push(id);
                }
                response[i].blankStars=blankStars;                              
              }         
            } 
            return response;
          },
          getSalonStars:function(response)
          {
            response.fullStars=[];
            response.blankStars=[];
            response.halfStar=false;
            if (response.salonRating) 
            {
              response.salonRating=parseFloat(response.salonRating);
              response.salonRating=response.salonRating.toFixed(1);
              var fullStars=[];
              var blankStars=[];
              var fullRatingFloat=response.salonRating; 
              var fullStar=parseInt(response.salonRating);
              var halfStar= fullRatingFloat-fullStar;
              var blankStar=5-fullStar;
              for (var a = 0; a < fullStar; a++) 
              {
                var id=a;
                fullStars.push(id);
              }
              if (halfStar>0) 
              {
                response.halfStar=true;
                blankStar=blankStar-1;              
              }            
              for (var b = 0; b <blankStar ; b++) 
              {
                var id=b;
                blankStars.push(id);
              }
              response.fullStars=fullStars;
              response.blankStars=blankStars;                              
            }
            else
            {
              var blankStars=[];
              for (var c = 0; c < 5; c++) 
              {
                var id=c;
                blankStars.push(id);
              }
              response.blankStars=blankStars;                              
            }          
            return response;
          },
          getSalons:function()
          {
            var url= API_URL+"salons?lat="+SessionService.getLocation().latitude+"&lng="+SessionService.getLocation().longitude+"";
            $http.get(url)
            .success(
            function(response)
            {
              $rootScope.$broadcast('salons:fetched',response); 
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getFavouriteSalons:function()
          {
            var url= API_URL+"users/"+SessionService.getUser().id+"/favorites?&lat="+SessionService.getLocation().latitude+"&lng="+SessionService.getLocation().longitude+"";            
            $http.get(url)
            .success(
            function(response)
            {
              $rootScope.$broadcast('favourites:fetched',response); 
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getFeatures:function()
          {
            var url=API_URL+"features";
            $http.get(url)
            .success(
            function(response)
            {     
              $rootScope.$broadcast('features:fetched',response);                           
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getCategories:function()
          {
            var url=API_URL+"categories";
            $http.get(url)
            .success(
            function(response)
            {     
              $rootScope.$broadcast('categories:fetched',response);                           
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getTypes:function()
          {
            var url=API_URL+"types";
            $http.get(url)
            .success(
            function(response)
            {     
              $rootScope.$broadcast('types:fetched',response);                           
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getServices:function()
          {
            var url=API_URL+"services";
            $http.get(url)
            .success(
            function(response)
            {     
              $rootScope.$broadcast('services:fetched',response);                           
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getFilterSalons:function(url)
          {
            url+="&lat="+SessionService.getLocation().latitude+"&lng="+SessionService.getLocation().longitude+"";
            $http.get(url)
            .success(
            function(response)
            {
              $rootScope.$broadcast('filtered:salons:fetched',response); 
            })
            .error(
            function(response)
            {
              console.log(JSON.stringify(response));
            })
          },
          getSort:function(response)
          {
            response.sort(function(a, b) {
              return parseFloat(a.distance) - parseFloat(b.distance);
            });             
            return response;
          },
          getStars:function(response)
          {
            for (var i = 0; i < response.length; i++) 
            {
              if (!response[i].salonRating) 
              {
                response[i].salonRating=0;
              }  
              if (!response[i].startingPrice) 
              {
                response[i].startingPrice=0;
              }  
              if (response[i].doorStepServices) 
              {
                response[i].doorStepServices=1;
              }
              if (!response[i].doorStepServices) 
              {
                response[i].doorStepServices=0;
              } 
              response[i].fullStars=[];
              response[i].blankStars=[];
              response[i].halfStar=false;
              if (response[i].salonRating) 
              {
                response[i].salonRating=parseFloat(response[i].salonRating);
                response[i].salonRating=response[i].salonRating.toFixed(1);
                var fullStars=[];
                var blankStars=[];
                var fullRatingFloat=response[i].salonRating; 
                var fullStar=parseInt(response[i].salonRating);
                var halfStar= fullRatingFloat-fullStar;
                var blankStar=5-fullStar;
                for (var a = 0; a < fullStar; a++) 
                {
                  var id=a;
                  fullStars.push(id);
                }
                if (halfStar>0) 
                {
                  response[i].halfStar=true;
                  blankStar=blankStar-1;              
                }            
                for (var b = 0; b <blankStar ; b++) 
                {
                  var id=b;
                  blankStars.push(id);
                }
                response[i].fullStars=fullStars;
                response[i].blankStars=blankStars;                              
              }
              else
              {
                var blankStars=[];
                for (var c = 0; c < 5; c++) 
                {
                  var id=c;
                  blankStars.push(id);
                }
                response[i].blankStars=blankStars;                              
              }          
            }
            response.sort(function(a, b) {
              return parseFloat(a.distance) - parseFloat(b.distance);
            });             
            return response;            
          }
  }
})

.factory('SessionService',function($rootScope,API_URL,Utility,$http)
  {
    //var cordinate={latitude:26.449923,longitude:80.331874,city:"Kanpur"};
    var cordinate={};
    var gpsStatus=false;
    var type;
    var token;
    var user={};
    var login=false;
    var notificationType;
    var offerNumber=0;
    var imageChanged=false;
    var states = {};
    return{  
      setGPS: function(status)
      {
        gpsStatus = status;
      },
      getGPS: function()
      {
        return gpsStatus;
      },
      setLocation:function(value)
      {
        cordinate=value;
      },
      getLocation:function()
      {
        return cordinate;
      },
      setImageChanged:function()
      {
        imageChanged=true;
      },
      isImageChanged:function()
      {
        return imageChanged;
      },
      setLogin: function(value)
      {
        login = value;
      },      
      getLogin: function()
      {
        return login;
      },   
      setUser: function(value)
      {
        user = value;
      },      
      getUser: function()
      {
        return user;
      },
      setDeviceToken: function(deviceToken)
      {
        token = deviceToken;
      },      
      getDeviceToken: function()
      {
        return token;
      },
      setNotificationType: function(value)
      {
        notificationType = value;
      },
      getNotificationType: function()
      {
        return notificationType;
      },
      setType: function(value)
      {
        type=value;
      },
      getType: function()
      {
        return type;
      },      
      updateToken:function()
      {
        var user ={};
        user.notificationType = notificationType;
        user.deviceToken = token;
        var url=API_URL+"deviceTokens/";
        $http.post(url, user)
        .success(function(response)
        {  
          console.log('deviceTokens sumbmited');
        })
        .error(function(response) 
        { 
          /*alert('after token get 6');*/
          console.log('error');
        });
      }    
    }
});