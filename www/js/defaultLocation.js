angular.module('starter.controllers', [])
.controller('AppCtrl', function(BROWSER_MODE,$scope,$timeout,$window,$localStorage,API_URL,$http,$cordovaSocialSharing,$filter,$ionicLoading,$http,$ionicModal,Utility,SessionService,$state,$ionicHistory) 
{   
  //$scope.browserMode=SessionService.getBrowserMode();  
  $scope.$on('get:login', function(e,view)
  {
    $scope.browser=BROWSER_MODE;
    $scope.loginData={};    
    if (SessionService.getLogin()) 
    {      
      $scope.loginData=SessionService.getUser();
      $scope.userName=$scope.loginData.name;
      $scope.loginData.log=true;
    }
    else
    {
      $scope.loginData.log=false;
      if ($localStorage.user) 
      {
        $scope.loader=true;
        var url=API_URL+"sessions/?email="+$localStorage.user.email+"&password="+$localStorage.user.password+"";
        $http.get(url)
        .success(function(response)
        { 
          var user={};
          if (response.length>0) 
          {
            user= response[0];
            if (!user.imageUrl) 
            {
              user.imageUrl="img/logo.png" 
            }
            $localStorage.user=user;
            $scope.loginData=user;
            $scope.userName=user.name;
            SessionService.setLogin(true);
            SessionService.setUser(user);
            $scope.loginData.log=true;         
          } 
          else 
          {
            $scope.loginData.log=false;
            $scope.loader=false;             
          }          
        })
        .error(function(response) 
        {       
          //Utility.showToastMessage("error..");
          console.log('error');
        });
      }
    }    
  });

  $scope.$on('set:offer:number', function(e,offerNumber)
  {
    $scope.offerNumber=offerNumber;
  });

  $scope.logOut=function()
  {
    SessionService.setLogin(false);
    $localStorage.user=null;
    SessionService.setUser(null);
    $scope.loginData.log=false;
  };

  $scope.login = function() 
  {         
    $state.go('app.login',{view:'home'});
    $ionicHistory.nextViewOptions({
      disableBack: true
    }); 
  };

  $scope.goToMain = function() 
  {         
    $state.go('app.main');
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
  };

  $scope.myProfile = function() 
  {
    $state.go('app.myProfile');
    $ionicHistory.nextViewOptions({
      disableBack: true
    });      
  };
  $scope.showOffers = function() 
  {         
    $state.go('app.offers');
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
  };
  $scope.goToContact = function() 
  {         
    $state.go('app.contact');
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
  };
  $scope.goToAbout = function() 
  {         
    $state.go('app.about');
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
  };
  $scope.goToPlayStore=function()
  {
    if (device.platform == 'android' || device.platform == 'Android' || device.platform == 'amazon-fireos' ) {
      
      cordova.InAppBrowser.open("https://play.google.com/store/apps/details?id=in.proapptive.salonsnearby",'_system');
    } else {
      $window.open("http://goo.gl/L36bKV",'_system');
    }
  };
  var message="Find salons, spas & explore deals anywhere.\nGoogle Play - https://goo.gl/awwsPX\nApp Store - https://goo.gl/L36bKV \nWebsite - ";
  $scope.shareSocialMedia = function() 
  {
    $cordovaSocialSharing.share(message, null,null, "http://www.salonsnearby.com");
  };

  $scope.$on('share:social:media', function(e)
  {
    $scope.shareSocialMedia();
  });

  $scope.showFavorites=function()
  {
    if ($scope.loginData.log) 
    {
      $state.go('app.favorites');
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
    }
    else
    {
      Utility.showToastMessage("Please login first,,!")
      $state.go('app.login',{view:'home'});
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
    }
  }  
})

.controller('LoginCtrl', function(BROWSER_MODE,$scope,$rootScope,$localStorage,$ionicHistory,$ionicHistory,ImageService,$state,$timeout,$filter,API_URL,$ionicLoading,$http,$ionicModal,SessionService, $stateParams,Utility) 
{    
  $scope.signupData={};
  $scope.$on('$ionicView.beforeEnter', function() 
  {  
    $scope.browserMode=BROWSER_MODE;  
    $scope.option=1;
    $scope.login=true;
    $scope.signUp=false;
  }); 

  $scope.loginOption=function(value)
  {
    if (value==1) 
    {
      $scope.option=1;
      $scope.login=true;
      $scope.signUp=false;
    } 
    else
    {
      $scope.option=2;
      $scope.login=false;
      $scope.signUp=true;
      // $scope.loader=false;
    }
  };

  $scope.openCamera=function()
  {
    if (BROWSER_MODE) 
    {
      $scope.signupData.imageUrl="img/logo.png";
      Utility.showToastMessage("Please download our app or signup without your image..");
      return;
    }
    //$scope.signupData.imageUrl="img/gents1.jpg";
    ImageService.camera(false);
  };

  $scope.$on('image:captured', function(e, image)
  {
   /* alert(image);*/
    $timeout(function() 
    {
      $scope.signupData.imageData=image;
      $scope.signupData.imageUrl =image;
    }, 100);    
  });

  $scope.loginData={};
  $scope.doLogin=function()
  {
    if(!$scope.loginData.email)
    {
      Utility.showToastMessage("Please Enter Your Email");
    }
    else if(!$scope.loginData.password)
    {
      Utility.showToastMessage("Please Enter Your Password")
    }
    else
    { 
      $scope.loader=true;
      var url=API_URL+"sessions/?email="+$scope.loginData.email+"&password="+$scope.loginData.password+"";
      $http.get(url)
      .success(function(response)
      { 
        var user={};
        if (response.length>0) 
        {
          user= response[0];
          if (!user.imageUrl) 
          {
            user.imageUrl="img/logo.png" 
          }
          $localStorage.user=user;
          SessionService.setLogin(true);
          SessionService.setUser(user);
          /**/
          $rootScope.$broadcast("get:login");
          $ionicHistory.goBack();
          if ($stateParams.view=="home") 
          {
            $state.go('app.main');
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
          }
          /**/          
        } 
        else 
        {
          $scope.loader=false;   
          Utility.showToastMessage("Invalid Credentials..!");
          return;
        }  
        $scope.loginData={}; 
        $scope.loader=false;   
        //$state.go("app.main");
      })
      .error(function(response) 
      {       
        Utility.showToastMessage("error..");
        console.log('error');
      });
    }   
  };

  $scope.forgotPassword=function()
  {
    if(!$scope.loginData.email)
    {
      Utility.showToastMessage("Please Enter Your Email");
    }
    else
    { 
      $scope.loader=true;
      var url=API_URL+"mails/forgot?email="+$scope.loginData.email;
      $http.get(url)
      .success(function(response)
      { 
        if (response) 
        {
          $scope.loader=false;   
          Utility.showToastMessage("Please Check The E-mail..!");
          return;     
        } 
        else 
        {
          $scope.loader=false;   
          Utility.showToastMessage("Invalid Credentials..!");
          return;
        }  
        $scope.loginData={};
      })
      .error(function(response) 
      {       
        Utility.showToastMessage("error..");
        console.log('error');
      });
    }   
  };

  $scope.signUp1=function()
  { 
    /*if (!BROWSER_MODE) 
    {
      if (!SessionService.getInternetState()) 
      {
        Utility.showToastMessage("There is no internet connection..");
        return;
      }
    }*/
    if(!$scope.signupData.name)
    {
      Utility.showToastMessage("Enter Your Name please...!");
      return;
    }
    if(!$scope.signupData.gender)
    {
      Utility.showToastMessage("please select any gender..!");
      return;
    }
    /*if(!$scope.signupData.mobile)
    {
      Utility.showToastMessage("Enter Mobile Number please..!");
      return;
    }*/
    if ($scope.signupData.mobile) 
    {
      if($scope.signupData.mobile.toString().length<9 || $scope.signupData.mobile.toString().length>10)
      {
        Utility.showToastMessage("Mobile number is not valid");
        return;
      }
    }    
    if(!$scope.signupData.email)
    {
      Utility.showToastMessage("Enter Your Email....!");
      return;
    } 
    if(!$scope.signupData.password)
    {
      Utility.showToastMessage("Enter Your password please...!");
      return;
    }
    $scope.loader=true;
    $scope.signupData.deviceToken=SessionService.getDeviceToken();
    $scope.signupData.notificationType="gcm";
    $scope.signupData.loginType="S";
    if($scope.signupData.imageUrl)
    {
      var imageNumber = Utility.getRandomStringWithLength(6);
      ImageService.uploadImage($scope.signupData.imageUrl,imageNumber); 
    }
    else
    {
      insertData();
    }     
  };
  $scope.$on('imageFile:uploaded', function(e, imageNumber)
  {
    $scope.signupData.imageUrl="https://s3-ap-southeast-1.amazonaws.com/salonfiles/images/"+imageNumber+".jpeg";    
    insertData();
  });
  var insertData=function()
  {
    var url=API_URL+"users/";
    $http.post(url, $scope.signupData)
    .success(function(response)
    { 
      if (response.id) 
      {
        Utility.showToastMessage("Thanks for registering with salonsnearby..");                
      }
      else 
      {
        Utility.showToastMessage("Already Exists..!,Please login with your credentials or sign up with different credentials.");        
      }
      /*$scope.signupData={};*/
      $scope.loader=false;     
    })
    .error(function(response) 
    {       
      $scope.loader=false;
      Utility.showToastMessage("error..");
    });
  }  

  $scope.fbSignup=function()
  {
    /*if (!BROWSER_MODE) 
    {
      if (!SessionService.getInternetState()) 
      {
        Utility.showToastMessage("There is no internet connection..");
        return;
      }
    }*/
    facebookConnectPlugin.getLoginStatus(function(response)
    {
      //alert("login status"+JSON.stringify(response));
      if(response.status=="connected" || response.status=="not_authorized")
      {
        fbLoginSuccess(response);
      }
      else
      {
        facebookConnectPlugin.login(["public_profile","email"],fbLoginSuccess,
        function (error) 
        { 
          //alert("login Failed"+JSON.stringify(error));
          $scope.loader=false;
        }); 
      }
    }, 
    function (error) 
    { 
      //alert("login Failed"+JSON.stringify(error));
      $scope.loader=false;
    });
  };

  var fbLoginSuccess = function (userData) 
  {
    var token = userData.authResponse.accessToken;
    facebookConnectPlugin.api('/me?fields=email,first_name,last_name,gender,age_range,picture&access_token=' + token, null,
    function (response) 
    {
      //alert("Token"+JSON.stringify(response));
      var gender=response.gender;
      if(gender=='male')
      {
        gender='M';
      }
      else{
        gender='F';
      }
      var user={};        
      user.name=response.first_name+" "+response.last_name;
      //alert(user);
      user.gender=gender;
      user.loginType="F";
      user.uniqueId=response.id;
      user.email=response.email;
      user.deviceToken=SessionService.getDeviceToken();
      user.imageUrl="http://graph.facebook.com/"+user.uniqueId+"/picture?type=large";
      user.notificationType=SessionService.getNotificationType();
      /*alert(user);*/
      var url=API_URL+"users/";
      $http.post(url, user)
      .success(function(response)
      {  
        //alert(JSON.stringify(response));
        user.id = response;
        //alert("userId is "+JSON.stringify(user.id));
        $localStorage.user=user;
        SessionService.setUser(user);
        SessionService.setLogin(true);
        console.log('Login successfully..');
        Utility.showToastMessage("Thanks for registering with salonsnearby...");
        /*$state.go('app.main');*/
        $scope.loader=false;
        $rootScope.$broadcast("get:login");
        $ionicHistory.goBack();  
        if ($stateParams.view=="home") 
        {
          $state.go('app.main');
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
        }     
      })
     .error(function(response) 
      {       
        /*alert("fb login successfull but not created in database");*/
        Utility.showToastMessage("error..");
        $scope.loader=false;
        console.log('error');
      });            
    },
    function (response) 
    {
      //alert("Token: " + JSON.stringify(response));
      Utility.showToastMessage("FB Login Failed");
      $scope.loader=false;
      console.log(response);
    });
  }

  $scope.googleSignup=function()
  {
    /*if (!BROWSER_MODE) 
    {
      if (!SessionService.getInternetState()) 
      {
        Utility.showToastMessage("There is no internet connection..");
        return;
      }
    }*/
    //alert("google clicked");
    window.plugins.googleplus.login({
         //'webClientId': '359996432822-lv533s1p292thh4j2be6seekuig5gnp1.apps.googleusercontent.com'
        // 'scopes': '... ', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        // 'webClientId': '577378579768-ogn52rh272ee1dpnm1phc5tdsfmnd5vm.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        // 'offline': true, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      },
      function (obj) 
      {
        //alert(JSON.stringify(obj)); // do something useful instead of alerting
        var user={};  
        console.log(JSON.stringify(obj));
        user.loginType="G";
        //$scope.fbDetails = obj;
        user.name= obj.displayName;
        var gender=obj.gender;
        if(gender=='male')
        {
          gender='M';
        }
        else{
          gender='F';
        }
        user.gender=gender;
        //$scope.fbDetails.age_range = obj.ageRangeMin;
        user.imageUrl = obj.imageUrl;
        user.uniqueId = obj.userId;
        user.email = obj.email;
        user.deviceToken=SessionService.getDeviceToken();
        user.notificationType=SessionService.getNotificationType();

        $scope.loader=true;
        $http.post(API_URL+ "users/",user)
        .success(function(response, headers)
        {
          //alert(JSON.stringify(response));
          user.id = response;
          //alert("userId is "+JSON.stringify(user.id));
          $localStorage.user=user;
          SessionService.setUser(user);
          SessionService.setLogin(true);
          console.log('Login successfully..');
          Utility.showToastMessage("Thanks for registering with salonsnearby...");
          $scope.loader=false;
          $rootScope.$broadcast("get:login");
          $ionicHistory.goBack();  
          if ($stateParams.view=="home") 
          {
            $state.go('app.main');
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
          }                  
        }) 
        .error(function(data) 
        { 
          console.log('error');
        }); 
      },
      function (msg) 
      {
        alert('error: ' + msg);
        console.log('error: ' + msg);
      });
  }; 
})

.controller('MainCtrl', function($timeout,$interval,SalonService,OfferService,BROWSER_MODE,$scope,$ionicHistory,$rootScope,$ionicModal,$ionicScrollDelegate,$state,$window,API_URL,$filter,$ionicLoading,$http,SessionService,Utility,GeoLocationService) 
{    
  $scope.deals=[];
  $scope.filter={};
  $scope.doorStepServices={};
  $scope.doorStepServices.checked=false;
  var filterItem={};
  var cordinates={};
  filterItem.doorStepServices=false;  
  $scope.hide=true;
  $scope.browser=BROWSER_MODE;
  var locationModalOpened=false;
  $scope.noLocation=false;
  $scope.downloadApp=function(url)  
  {
    $window.open(url,'_system')
  };
  $scope.hideDownload=function()
  {
    $scope.download=false;
  }  
  $scope.$on('$ionicView.beforeEnter', function()
  {
    if (SessionService.getLocation().latitude) 
    {
      console.log("Saved location fetched form session\n");
      $timeout(function() {
        $scope.city=SessionService.getLocation().city;        
      }, 100);
      $scope.loader=true;
      OfferService.getOffers();
    }
    else if (!BROWSER_MODE && !SessionService.getGPS())
    {
      console.log("location not fetched, trying to get location.\n");
      $scope.fetchingLocation=true;
      SessionService.setGPS(false);
      getLocation();
    }
    if (BROWSER_MODE) 
    {
      $scope.download=true;      
      $scope.fetchingLocation=false;
      $scope.locationFailed=false;
      SessionService.setGPS(true);
      $scope.city="Kanpur";
      SessionService.setLocation({latitude:26.449923,longitude:80.331874,city:"Kanpur"}); 
      $scope.loader=true;       
      OfferService.getOffers();
    }
    firstTime=true;
    $scope.browser=BROWSER_MODE;  
    $rootScope.$broadcast('get:login'); 
    $scope.log=SessionService.getLogin();
    cordinates=SessionService.getLocation();
    $scope.filter={};
    filterItem={};
    $scope.doorStepServices.checked=false;
    $scope.loader=true;
    $scope.deals=[];
    //OfferService.getOffers(); 
    OfferService.getAdvertisements();  
  });

  $scope.$on('internet:connected', function(e)
  {    
    if (BROWSER_MODE) 
    {
      $scope.download=true;      
      $scope.noLocation=false;
      SessionService.setGPS(true);
      if (SessionService.getLocation().latitude) 
      {
        $timeout(function() {
          $scope.city=SessionService.getLocation().city;        
        }, 100);        
      }
      else
      {
        $scope.city="Kanpur";
        SessionService.setLocation({latitude:26.449923,longitude:80.331874,city:"Kanpur"});        
      }
      $scope.loader1=true;
      OfferService.getOffers();
    }
    else
    {
      if (SessionService.getLocation().latitude && ($scope.deals.length==0))
      {
        $scope.loader1=true;
        OfferService.getOffers();
      }
    }
    firstTime=true;
    $scope.browser=BROWSER_MODE;  
    $rootScope.$broadcast('get:login'); 
    $scope.log=SessionService.getLogin();
    cordinates=SessionService.getLocation();
    $scope.filter={};
    filterItem={};
    $scope.doorStepServices.checked=false;
    if ($scope.advertisements.length==0) 
    {
      $scope.loader=true;
      $scope.deals=[];
      OfferService.getAdvertisements();
    }
  });

  $scope.$on('offers:fetched', function(e,offers)
  {
    $scope.loader1=false;
    alert("offers catched");
    for (var i = 0; i < offers.length; i++) 
    {
      offers[i].validTill=$filter('date')(new Date(offers[i].validTill), 'dd/MM/yyyy');
      if (offers[i].salonRating==null) 
      {
        offers[i].salonRating="3.0";
      }
    } 
    $scope.deals=offers;
    $scope.deals.sort(function(a, b) {
      return parseFloat(a.distance) - parseFloat(b.distance);
    });
    $rootScope.$broadcast('set:offer:number',$scope.deals.length);         
  });

  $scope.$on('advertisements:fetched', function(e,response)
  {
    var images=[];
    for (var i = 0; i < response.length; i++) 
    {
      var image={};
      image.imageUrl=response[i].imageUrl;
      if (response[i].type=='O') 
      {
        image.url=response[i].url;
      }
      if (response[i].type=='S') 
      {
        image.name=response[i].name;
        image.salonId=response[i].salonId;
      }
      images.push(image);
    }   
    $scope.images=images;   
    $scope.advertisements=response; 
    $scope.loader=false;
  });

  $scope.$on('geoLocation:fetch:success', function(e,lat, lng,city)
  { 
    console.log("location success:---");
    $interval.cancel(timer);
    $scope.fetchingLocation=false;
    $scope.city=city;
    console.log($scope.city);
    cordinates.latitude=lat;
    cordinates.longitude=lng;    
    cordinates.city=$scope.city;
    SessionService.setGPS(true);
    SessionService.setLocation(cordinates);
    $scope.loader1=true;
    OfferService.getOffers();
  });

  $scope.$on('geoLocation:fetch:failure', function()
  {       
    console.log("location not fetched..\n");
    if (BROWSER_MODE) 
    {
      $scope.loader1=true;
      $scope.city="Kanpur"
      $scope.fetchingLocation=false;
      SessionService.setGPS(true);
      SessionService.setLocation({latitude:26.449923,longitude:80.331874,city:"Kanpur"});
      OfferService.getOffers();
    }
    else if (locationCounter>=10)
    {
      console.log("location not fetched.. and counter finished\n");
      $scope.loader1=true;
      $interval.cancel(timer);
      $scope.fetchingLocation=false;
      SessionService.setGPS(true);
      $scope.city="Kanpur";
      SessionService.setLocation({latitude:26.449923,longitude:80.331874,city:"Kanpur"});        
      OfferService.getOffers();
    } 
  }); 

  $scope.getDetails=function(image,view)
  {
    if (image.salonId) 
    {
      if (view=='deals') 
      {
        $state.go('app.salonDetail',{id:image.salonId,name:image.name,view:view});
      } 
      else 
      {
        $state.go('app.salonDetail',{id:image.salonId,name:image.name});
      }      
    } 
    else 
    {
      $window.open(image.url,'_system');
    }
  };

  var locationCounter=0;
  var timer;
  var getLocation=function()
  {
    timer=$interval(function() {
      console.log("location fetching:--"+locationCounter);
      locationCounter++;
      GeoLocationService.getGeoLocation();
    }, 1000);    
  };

  $scope.myLocation=function()
  {
    SessionService.setGPS(false);
    $scope.fetchingLocation=true;
    getLocation();    
    if (locationModalOpened) {$scope.locationModal.hide();}    
  };

  $scope.saveFilter=function(value)
  {
    if (!SessionService.getGPS()) 
    {
      Utility.showToastMessage("Please wait for detect location..");
      return;
    }
    filterItem.doorStepServices=false;  
    cordinates=SessionService.getLocation();
    if (value=='favorites') 
    {
      if (SessionService.getLogin()) 
      {
        $state.go("app.favorites");
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        return;
      } 
      else 
      {
        Utility.showToastMessage("Please login first to see your favorite salons,,!")
        $state.go('app.login',{view:'home'});
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        return;
      }      
    }  
    if (value=='M' || value=='F' || value=='U') 
    {
      filterItem.gender=value;
    } 
    if (value=='doorstep') 
    {
      filterItem.doorStepServices=true;
    }
    SessionService.setFilters(filterItem);
    $state.go("app.listing",{city:$scope.city});
  };

  $ionicModal.fromTemplateUrl('templates/location-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function($ionicModal) 
  {
    $scope.locationModal = $ionicModal;
  });
  $scope.search={};

  $scope.location = function()
  {
    locationModalOpened=true;
    $scope.locationModal.show();
    $scope.predictions = null;
    $scope.search.location = null;
  };

  $scope.locationTextChanged = function()
  {
    if($scope.search.location.length>0)
    {
      service = new google.maps.places.AutocompleteService();
      var request = {
        input: $scope.search.location,
        componentRestrictions: {country: 'IN'},
      };
      service.getPlacePredictions(request, callback);
    }
    else
    {
      $scope.predictions = null;
    }
  };

  function callback(predictions, status)
  {
    if (status == google.maps.places.PlacesServiceStatus.OK) 
    {
      $scope.predictions = predictions;
      $scope.showLocationList=true;
    }
  };

  $scope.showLocationList=false;
  $scope.selectLocation = function(location)
  {
    geocoder = new google.maps.Geocoder();
    var address = location.description;
    geocoder.geocode( { 'address': address}, function(results, status) 
    {
      if (status == google.maps.GeocoderStatus.OK) 
      {
        center = { "latitude": results[0].geometry.location.lat(), "longitude": results[0].geometry.location.lng()};
        cordinates.latitude=center.latitude;
        cordinates.longitude=center.longitude;
        cordinates.city=address;
        $scope.city=address;
        $scope.deals=[];
        $scope.loader1=true;
        SessionService.setGPS(true);
        SessionService.setLocation(cordinates);
        OfferService.getOffers();
      } 
      else 
      {
        alert("Loading was not successful for the following reason: " + status);
      }
    });
    $scope.search.location=address;
    $scope.locationModal.hide();
    $scope.showLocationList=false;
  };

  $ionicModal.fromTemplateUrl('templates/search-salon-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function($ionicModal) 
  {
    $scope.searchModal = $ionicModal;
  });

  $scope.openSearchmodel=function()
  {
    if (!SessionService.getGPS()) 
    {
      Utility.showToastMessage("Please wait for detect location..");
      return;
    }
    $scope.searchModal.show();
    $scope.modalsalons = [];  
    $scope.loader=true;
    SalonService.getSalons();  
  };

  $scope.$on('salons:fetched', function(e,response)
  {
    $scope.modalsalons=SalonService.getStars(response);
    $scope.modalsalons=SalonService.getSort($scope.modalsalons);
    $scope.loader=false; 
  });
  
  $scope.getSalonDetails=function(salon)
  {
    $scope.searchModal.hide();
    var saloon= salon;
    $state.go('app.salonDetail',{id:salon.id,name:salon.name});    
  };
  $scope.dial = function(mobile)
  {
    document.location.href = "tel:" + "+91"+mobile;
  };
})

.controller('ListingCtrl', function($scope,BROWSER_MODE,SalonService,$timeout,$state,$ionicScrollDelegate,API_URL,$http, GeoLocationService,$rootScope,$interval,$filter,$ionicLoading,$http,$ionicModal,Utility,SessionService, $stateParams,GeoLocationService) 
{
  $scope.filter={};
  $scope.showDeals=true;  
  $scope.showfeature = false; 
  $scope.saloons=[];
  $scope.features=[];
  $scope.categories=[];
  $scope.types=[];
  $scope.services=[];   
  $scope.gender=SessionService.getFilters().gender;
  var filterItem=SessionService.getFilters();                
  $scope.doorStepServices={};
  $scope.doorStepServices.checked=SessionService.getFilters().doorStepServices;
  $scope.city=SessionService.getLocation().city;
  var cordinates={};
  var totalSaloons=[];
  $scope.noMoreItems = false;
  var filterOpened=false;
  $scope.browser=BROWSER_MODE;
  $scope.doorStepServices.checked=SessionService.getFilters().doorStepServices;  
  $scope.loader=true;
  SalonService.getFeatures();
  $scope.search={};
  var locationModalOpened=false;
  $scope.showLocationList=false;

  $scope.saveFilter=function()
  {
    totalSaloons=[];
    $scope.noMoreItems = false;
    $scope.completed=false;
    $scope.loader=true;
    $scope.saloons=[];
    var features=[];
    var services=[]; 
    var feature;
    var service;
    var url= API_URL+"salons?";
    url +="&gender='"+$scope.gender+"'";
    if ($scope.doorStepServices.checked) 
    {
      url +="&doorStepServices=true"
    }    
    for (var i = 0; i < $scope.features.length; i++) 
    {
      if ($scope.features[i].checked) 
      {
        features.push($scope.features[i].id);
      }
    }

    feature = "";
    for(i=0,j=0,feature=""; i<features.length; i++){
      if(features[i]){
        feature += ((j==0)?"":",") +"'"+features[i]+"'";
         j++;
      }
    }    
    for (var i = 0; i < $scope.services.length; i++) 
    {
      if ($scope.services[i].checked) 
      {
        services.push($scope.services[i].id);        
      }
    }
    service = "";
    for(i=0,j=0,service=""; i<services.length; i++){
      if(services[i]){
        service += ((j==0)?"":",") +"'"+services[i]+"'";
         j++;
      }
    }        
    if ($scope.filter.typeId) 
    {
      url += "&typeId="+$scope.filter.typeId;
    }
    if ($scope.filter.categoryId) 
    {
      url += "&categoryId="+$scope.filter.categoryId;
    }   
    if (features.length>0) 
    {
      url +="&features="+feature;
    } 
    if (services.length>0) 
    {
      url +="&services="+service;
    }  
    if (filterOpened){$scope.closeFilterModal();} 
    console.log(JSON.stringify($scope.saloons));
    SalonService.getFilterSalons(url);         
    console.log(url);        
  };
  $scope.$on('internet:connected', function(e)
  {
    if (totalSaloons.length==0) 
    {
      $scope.loader=true;
      SalonService.getFeatures();
      $scope.saveFilter();   
    }
  });
  $scope.saveFilter();
  $scope.$on('features:fetched', function(e,response)
  { 
    $scope.features=response;
    SalonService.getCategories();
  });
  $scope.$on('categories:fetched', function(e,response)
  { 
    for (var i = 0; i < response.length; i++) 
    {
      response[i].checked=false;
    }
    $scope.categories=response;
    SalonService.getTypes();
  });
  $scope.$on('types:fetched', function(e,response)
  { 
    for (var i = 0; i < response.length; i++) 
    {
      response[i].checked=false;
    }
    $scope.types=response; 
    SalonService.getServices();
  });
  $scope.$on('services:fetched', function(e,response)
  { 
    $scope.services=response; 
  });

  $scope.dial = function(mobile)
  {
    document.location.href = "tel:" + "+91"+mobile;
  };

  $scope.selectType=function(index)
  {
    for (var i = 0; i < $scope.types.length; i++) 
    {
      $scope.types[i].checked=false;
    }
    $scope.types[index].checked=true;
    $scope.filter.typeId=$scope.types[index].id;
  };

  $scope.selectCategory=function(index)
  {
    for (var i = 0; i < $scope.categories.length; i++) 
    {
      $scope.categories[i].checked=false;
    }
    $scope.categories[index].checked=true;
    $scope.filter.categoryId=$scope.categories[index].id;
  };

  $scope.getDetails=function(salon)
  {
   if (salon.salonId) 
    {
      $state.go('app.salonDetail',{id:salon.salonId,name:salon.name});
    } 
    else 
    {
      $state.go('app.salonDetail',{id:salon.id,name:salon.name});
    }    
  };

  $scope.selectGender=function(value)
  {
    filterItem.gender=value;
    $scope.gender=value;
  };

  $scope.getSort=function(value,id)
  {
    $scope.option=id;
    if (value=='priceLowtoHigh') 
    {
      $scope.saloons.sort(function(a, b) {
          return parseFloat(a.startingPrice) - parseFloat(b.startingPrice);
      });
    }
    else if (value=='priceHighttoLow') 
    {
      $scope.saloons.sort(function(a, b) {
          return parseFloat(b.startingPrice) - parseFloat(a.startingPrice);
      });      
    }
    else if (value=='distance') 
    {
      $scope.saloons=SalonService.getSort($scope.saloons);
    }
    else if (value=='offerNumber') 
    {
      $scope.saloons.sort(function(a, b) {
          return parseFloat(b.offerNumber) - parseFloat(a.offerNumber);
      });
      $scope.reverse = false;
    }
    else if (value=='salonRating') 
    {
      $scope.saloons.sort(function(a, b) {
          return parseFloat(b.salonRating) - parseFloat(a.salonRating);
      });
    } 
    else if (value=='doorstepService') 
    {
      $scope.saloons.sort(function(a, b) {
          return parseFloat(b.doorStepServices) - parseFloat(a.doorStepServices);
      });
    }  
    $scope.showSort=false; 
    $scope.closeFilterModal();
  };

  $ionicModal.fromTemplateUrl('templates/search-salon-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function($ionicModal) 
  {
    $scope.searchModal = $ionicModal;
  });
  $scope.$on('salons:fetched', function(e,response)
  {             
    $scope.modalsalons=SalonService.getStars(response);
    $scope.modalsalons=SalonService.getSort($scope.modalsalons);
    $scope.loader=false; 
  });
  $scope.seachSalonOrServices = function()
  {
    $scope.modalsalons = [];  
    $scope.loader=true;
    SalonService.getSalons();    
  };

  $scope.getSalonDetails=function(salon)
  {
    $scope.searchModal.hide();
    var saloon= salon;
    $state.go('app.salonDetail',{id:salon.id,name:salon.name});    
  };

  $scope.getCategory=function(categoryId)
  {
    $scope.filter.categoryId=categoryId;
  };

  $scope.getType=function(typeId)
  {
    $scope.filter.typeId=typeId; 
  };

  $ionicModal.fromTemplateUrl('templates/filter-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) 
  {
    $scope.filterModal = modal;    
  });

  $scope.openFilterModal = function()
  {
    $scope.filterModal.show();
    filterOpened=true;
    $ionicScrollDelegate.$getByHandle('messageScroll').scrollTop();     
  };

  $scope.closeFilterModal = function()
  {
    $scope.filterModal.hide();
    filterOpened=false;
  };
  
  $scope.$on('filtered:salons:fetched', function(e,response)
  {    
    $scope.totallength=response.length;
    totalSaloons=SalonService.getSort(response);
    $scope.loader=false;
  });
  $scope.noMoreItems = false;
 
  $scope.loadMore=function()
  {
    var salonLength = $scope.saloons.length;
    var remaining=totalSaloons.length-salonLength;
    if (remaining<10) 
    {      
      for (var i=salonLength; i<salonLength+remaining; i++) 
      {
        $scope.saloons[i]=totalSaloons[i];
      }
      $scope.completed=true;
      $scope.noMoreItems = true;
    }
    else 
    {
      for (var i=salonLength; i<salonLength+10; i++) 
      {
        $scope.saloons[i]=totalSaloons[i];
      }
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };
  $scope.saloons=[];
  $ionicModal.fromTemplateUrl('templates/location-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function($ionicModal) 
  {
    $scope.locationModal = $ionicModal;
  });
  
  $scope.location = function()
  {
    locationModalOpened=true;
    $scope.locationModal.show();
    $scope.predictions = null;
    $scope.search.location = null;
  };

  $scope.locationTextChanged = function()
  {
    if($scope.search.location.length>0)
    {
      service = new google.maps.places.AutocompleteService();
      var request = {
        input: $scope.search.location,
        componentRestrictions: {country: 'IN'},
      };
      service.getPlacePredictions(request, callback);
    }
    else
    {
      $scope.predictions = null;
    }
  };

  function callback(predictions, status)
  {
    if (status == google.maps.places.PlacesServiceStatus.OK) 
    {
      $scope.predictions = predictions;
      $scope.showLocationList=true;
    }
  };

  $scope.selectLocation = function(location)
  {
    geocoder = new google.maps.Geocoder();
    var address = location.description;
    geocoder.geocode( { 'address': address}, function(results, status) 
    {
      if (status == google.maps.GeocoderStatus.OK) 
      {
        center = { "latitude": results[0].geometry.location.lat(), "longitude": results[0].geometry.location.lng()};
        cordinates.latitude=center.latitude;
        cordinates.longitude=center.longitude;
        cordinates.city=address;
        SessionService.setLocation(cordinates);
        $scope.saloons=[];
        $scope.loader=true;
        $scope.saveFilter();  
      }
      else
      {
        alert("Loading was not successful for the following reason: " + status);
      }
    });
    $scope.city=address;
    $scope.search.location=address;
    $scope.locationModal.hide();
    $scope.showLocationList=false;    
  };

  $scope.myLocation=function()
  { 
    GeoLocationService.getGeoLocation();
    if (locationModalOpened) {$scope.locationModal.hide();}    
  };

  $scope.$on('geoLocation:fetch:success', function(e,lat, lng,city)
  { 
    $scope.city=city;
    console.log($scope.city);
    cordinates.latitude=lat;
    cordinates.longitude=lng;    
    cordinates.city=$scope.city;
    SessionService.setLocation(cordinates);
    SessionService.setGPS(true);
    offset=0;
    $scope.saloons=[];
    $scope.loader=true;
    $scope.saveFilter();  
  });

  $scope.openSearchmodel=function()
  {
    if (!SessionService.getGPS()) 
    {
      Utility.showToastMessage("Please wait for detect location..");
      return;
    }
    $scope.searchModal.show();
    $scope.modalsalons = [];  
    $scope.loader=true;
    SalonService.getSalons();  
  };
})

.controller('OffersCtrl', function($scope,$state,SalonService,OfferService,$filter,API_URL,$http,SessionService) 
{
  $scope.deals=[];
  var cordinates={};
  $scope.$on('$ionicView.beforeEnter', function()
  {
    $scope.loader=true;    
    OfferService.getOffers();  
  });
  $scope.$on('internet:connected', function(e)
  {
    if ($scope.deals.length==0) 
    {
      $scope.loader=true;
      OfferService.getOffers();      
    }
  });
  $scope.$on('offers:fetched', function(e,offers)
  {
    //alert("offers catched");
    for (var i = 0; i < offers.length; i++) 
    {
      offers[i].validTill=$filter('date')(new Date(offers[i].validTill), 'dd/MM/yyyy');
      if (offers[i].salonRating==null) 
      {
        offers[i].salonRating="3.0";
      }
    } 
    $scope.deals=offers;
    $scope.deals.sort(function(a, b) {
      return parseFloat(a.distance) - parseFloat(b.distance);
    });
    $scope.loader=false;
    $rootScope.$broadcast('set:offer:number',$scope.deals.length);         
  });

  $scope.getDetails=function(salon)
  {
    var view= 'deals';
    $state.go('app.salonDetail',{id:salon.salonId,name:salon.name,view:view});       
  };

  $scope.dial = function(mobile)
  {
    document.location.href = "tel:" + "+91"+mobile;
  };
})

.controller('FavoritesCtrl', function($scope,$state,SalonService,API_URL,$http,GeoLocationService,$ionicLoading,$ionicModal,Utility,SessionService, $stateParams) 
{
  $scope.saloons=[];
  var userId;
  $scope.loader=true;
  SalonService.getFavouriteSalons();  
  $scope.$on('favourites:fetched', function(e,response)
  {
    $scope.saloons=SalonService.getStars(response);
    $scope.saloons=SalonService.getSort($scope.saloons);
    $scope.loader=false;
  });
  $scope.$on('internet:connected', function(e)
  {
    if ($scope.saloons.length==0) 
    {
      $scope.loader=true;
      SalonService.getFavouriteSalons();    
    }
  });
  $scope.getDetails=function(salon)
  {
    $state.go('app.salonDetail',{id:salon.salonId,name:salon.name});    
  };

  $scope.dial = function(mobile)
  {
    document.location.href = "tel:" + "+91"+mobile;
  };
})

.controller('SalonDetailCtrl', function(BROWSER_MODE,SalonService,$scope,$state,$window,$rootScope,$filter,$ionicSlideBoxDelegate,SessionService,$ionicScrollDelegate,$cordovaSocialSharing,$ionicPopover,$http,API_URL,$timeout,$ionicLoading,$ionicModal,Utility,$stateParams,GeoLocationService) 
{
  $scope.option=1;
  $scope.information=true;
  $scope.rateCard=false;
  $scope.deals=false;
  $scope.saloon={};
  $scope.salonName="Details";
  $scope.salonName=$stateParams.name;
  $scope.weekdays=[];
  var cordinates={};
  $scope.review={};
  $scope.reviews=[];
  $scope.report={};
  $scope.rateimages=[];
  var gotDetail=false;
  if (BROWSER_MODE) 
  {
    var shareTemplate='<ion-popover-view style="height: 65px;width: 120px"><ion-content><div class="list"><a class="item" ng-click="openReportModal()">Report Info</a></div></ion-content></ion-popover-view>';
  } 
  else 
  {
    var shareTemplate = '<ion-popover-view style="height: 120px;width: 120px"><ion-content><div class="list"><a class="item" ng-click="openReportModal()">Report Info</a><a class="item" ng-click="shareSocialMedia()">Share</a></div></ion-content></ion-popover-view>';
  }
    
  $scope.popoverMenu = $ionicPopover.fromTemplate(shareTemplate, {
    scope: $scope
  });
  
  $scope.openPopupMenu = function($event) 
  {
    $scope.popoverMenu.show($event);
  };

  $scope.shareSocialMedia = function() 
  {
    $scope.popoverMenu.hide();
    $rootScope.$broadcast('share:social:media');
  };

  $scope.goToLink=function(link)
  {
    $window.open(link,'_system');
  };
  $scope.$on('salon:detail:fetched', function(e,response)
  {    
    for (var i = 0; i < response.rateImages.length; i++) 
    {
      var image={};
      image.src=response.rateImages[i].url;
      $scope.rateimages.push(image);
    }
    $ionicSlideBoxDelegate.update(); 
    $scope.saloon=SalonService.getSalonStars(response);
    SalonService.getReviews($stateParams.id);
  });
  $scope.$on('reviews:fetched', function(e,response)
  {
    response=SalonService.getReviewStars(response); 
    $scope.reviewSecondary=[];
    for (var i = 0; i < response.length; i++) 
    {
      response[i].date=$filter('date')(new Date(response[i].date), 'M/d/yy h:mm a');
      if (i<2) 
      {
        $scope.reviewSecondary.push(response[i])
      }
    }
    $scope.reviews=response;
    $scope.loader=false;
    gotDetail=true;
  })
  $scope.$on('$ionicView.beforeEnter', function()
  { 
    if ($stateParams.view=='deals') 
    {
      $scope.option=3;
      $scope.information=false;
      $scope.rateCard=false;
      $scope.deals=true;
    }
    $scope.loader=true;
    SalonService.getSalonDetail($stateParams.id);    
  });
  $scope.$on('internet:connected', function(e)
  {
    if (!gotDetail) 
    {
      $scope.loader=true;
      SalonService.getSalonDetail($stateParams.id); 
    }
  });
  $scope.dial=function()
  {
    document.location.href = "tel:" + "+91"+$scope.saloon.contactNumber;
  };

  $scope.select=function(value)
  {
    if (value==1) 
    {
      $scope.option=1;
      $scope.information=true;
      $scope.rateCard=false;
      $scope.deals=false;
    } 
    else if (value==2) 
    {
      $scope.option=2;
      $scope.information=false;
      $scope.rateCard=true;
      $scope.deals=false;
    } 
    else if (value==3) 
    {
      $scope.option=3;
      $scope.information=false;
      $scope.rateCard=false;
      $scope.deals=true;
    }
  };

  $scope.showImages = function(image) 
  {   
    $scope.rateImage=image; 
    $scope.showModal('templates/rate-image-modal.html');
  };
   
  $scope.showModal = function(templateUrl) 
  {
    $ionicModal.fromTemplateUrl(templateUrl, 
    {
      scope: $scope
    }).then(function(modal) 
    {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
   
  $scope.closeModal = function() 
  {
    $scope.modal.hide();
    $scope.modal.remove()
  };

  $ionicModal.fromTemplateUrl('templates/all-review-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) 
  {
    $scope.readReviewModal = modal;
  });

  $scope.openReadReviewModal = function()
  {       
    $scope.readReviewModal.show();
    $ionicScrollDelegate.$getByHandle('messageScroll').scrollTop();
  };

  $scope.closeReadReviewModal = function()
  {
    $scope.readReviewModal.hide();
  };

  $ionicModal.fromTemplateUrl('templates/write-review-modal.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) 
  {
    $scope.writeReviewModal = modal;
  });

  $scope.submitReview=function()
  {
    var data={};
    if (!$scope.review.title) 
    {
      Utility.showToastMessage("Please write any review title...");
      return;
    }
    if (!$scope.review.comment) 
    {
      Utility.showToastMessage("Please write any review,your feedback is important to us to improve...");
      return;
    }
    $scope.loader=true;
    data.title=$scope.review.title;
    data.comment=$scope.review.comment;
    data.date=new Date();
    data.ratings=[];
    for (var i = 0; i < $scope.criterias.length; i++) 
    {
      for (var j = 0; j < $scope.criterias[i].stars.length; j++) 
      {
        if ($scope.criterias[i].stars[j].selected) 
        {
          $scope.criterias[i].score=$scope.criterias[i].stars[j].id;
        }        
      }
      var rating={};
      rating.criteriaId=$scope.criterias[i].id;
      rating.score=$scope.criterias[i].score;
      data.ratings.push(rating);
    }    
    var userId=SessionService.getUser().id;
    console.log(data);
    var url=API_URL+"salons/"+$stateParams.id+"/user/"+userId+"/reviews";
    $http.post(url, data)
    .success(function(data, headers)
    {
      SalonService.getReviews($stateParams.id);
      $scope.loader=false;
      console.log('review submitted successfully..');
      Utility.showToastMessage("Thanx..for your feedback!"); 
    })
    .error(function(data) 
    {      
      console.log('error');
      $scope.loader=false;
    });
    $scope.closeWriteReviewModal();
  };

  $scope.selectStars=function(starId,criteriaId)
  {    
    var parent;
    var index;
    var counter=1;
    for (var i = 0; i < $scope.criterias.length; i++) 
    {
      if ($scope.criterias[i].id==criteriaId) 
      {
        parent=i;
        for (var j = 0; j < $scope.criterias[i].stars.length; j++) 
        {
          if ($scope.criterias[i].stars[j].id==starId) 
          {
            index=j;
          }
        }
      }
    }
    for (var i = 0; i < $scope.criterias[parent].stars.length; i++) 
    {
      if (i<=index) 
      {
        $scope.criterias[parent].stars[i].selected=true;
      }
      else
      {
        $scope.criterias[parent].stars[i].selected=false; 
      }
    }
  };

  $scope.openWriteReviewModal = function()
  { 
    if (!SessionService.getLogin()) 
    {
      $state.go("app.login");
      return;
    }         
    var url=API_URL+"criteria/";
    $http.get(url)
    .success(
      function(response)
      {
        console.log(JSON.stringify(response)); 
        for (var i = 0; i < response.length; i++) 
        {
          response[i].stars=[];
          for (var j = 1; j < 6; j++) 
          {
            var star={};
            star.id=j;
            if (j<=4) 
            {
              star.selected=true;
              if (j==4) 
              {
                star.selected=false;
              }              
            } 
            else 
            {
              star.selected=false;
            }            
            response[i].stars.push(star);
          } 
        }                
        $scope.criterias=response; 
      })
    .error(
      function(response)
      {
        console.log(JSON.stringify(response));
      })
    $scope.writeReviewModal.show();
    $ionicScrollDelegate.$getByHandle('messageScroll').scrollTop();
  };

  $scope.closeWriteReviewModal = function()
  {
    $scope.writeReviewModal.hide();
  };

  $ionicPopover.fromTemplateUrl('timings.html', 
  {
    scope: $scope
  }).then(function(popover) {
    $scope.popoverTiming = popover;
  });

  $scope.openTimings = function($event) 
  {
    $scope.popoverTiming.show($event);
  };

  $scope.favorite=false;
  $scope.makeFavorite = function() 
  {
    var data={};
    if (!SessionService.getLogin()) 
    {
      $state.go("app.login");
      return;
    } 
    else
    {
      data.userId=SessionService.getUser().id;
    }
    data.salonId=$stateParams.id;
    var url=API_URL+"users/"+data.userId+"/favorites/"+$stateParams.id+"";
    $http.get(url)
    .success(function(response)
    {
      if (response.length>0) 
      {
        $scope.favorite=true;    
        Utility.showToastMessage("Already in your favorite list...!");
      } 
      else 
      {
        var url=API_URL+"users/"+data.userId+"/favorites";
        $http.post(url, data)
        .success(function(response)
        {      
          $scope.favorite=true;
          console.log('review submitted successfully..');
          Utility.showToastMessage("Thanx..to make favorite!");
        })
        .error(function(response) 
        {       
          Utility.showToastMessage("error..");
          console.log('error');
        });
      }            
    })
    .error(function(response) 
    {       
      Utility.showToastMessage("error..");
      console.log('error');
    });
    $scope.loader=false;
  };

  $scope.showRoute = function()
  {  
    cordinates=SessionService.getLocation();
    if (BROWSER_MODE) 
    {
      Utility.showToastMessage("Please download our app to use this feature..");
      return; 
    }
    var startLat = cordinates.latitude;
    var startLng = cordinates.longitude;
    var destinationLat = $scope.saloon.lat;
    var destinationLng = $scope.saloon.lng;
    launchnavigator.navigate([startLat, startLng], [destinationLat, destinationLng], successCallback, errorCallback);
  };

  function successCallback()
  {
    console.log("Success");
  };

  function errorCallback()
  {
    console.log("Error");
  };

  $ionicModal.fromTemplateUrl('templates/write-report-model.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) 
  {
    $scope.writeReportModal = modal;
  });

  $scope.report={};
  $scope.openReportModal = function()
  {
    $scope.popoverMenu.hide();
    if (!SessionService.getLogin()) 
    {
      $state.go("app.login");
      return;
    }    
    $scope.report={};
    $scope.writeReportModal.show();
  };

  $scope.submitReport=function()
  {    
    if (!$scope.report.title) 
    {
      Utility.showToastMessage("Please write any title for your issues...");
      return;
    }
    if (!$scope.report.issues) 
    {
      Utility.showToastMessage("Please write your issues,your feedback is important to us to improve...");
      return;
    }
    $scope.loader=true;
    var data={};
    data.userId=SessionService.getUser().id;
    data.date = new Date();
    data.title=$scope.report.title
    data.issues = $scope.report.issues;
    var url=API_URL+"salons/"+$stateParams.id+"/issues";
    $http.post(url, data)
    .success(function(response)
    {      
      console.log('review submitted successfully..');
      Utility.showToastMessage("Thanx..for your feedback..!");
    })
    .error(function(response) 
    {       
      Utility.showToastMessage("error..");
      console.log('error');
    });
    $scope.writeReportModal.hide();
    $scope.loader=false; 
  };
})

.controller('ProfileCtrl', function($scope,API_URL,Utility,ImageService,$state,$timeout,API_URL,$http,SessionService)
{  
  $scope.signupData={};
  $scope.$on('$ionicView.beforeEnter', function() 
  {
    $scope.loader=false;
    $scope.signupData=SessionService.getUser();
  });

  $scope.openCamera=function()
  {
    ImageService.camera(false);
    //$scope.signupData.imageUrl="img/gents1.jpg";
  };
  $scope.$on('image:captured', function(e, image)
  {
   /* alert(image);*/
    $timeout(function() 
    {
      $scope.signupData.imageData=image;
      $scope.signupData.imageUrl =image;
    }, 100);    
  });  

  $scope.update=function() 
  {
    $scope.loader=true; 
    var imageChanged=SessionService.isImageChanged();
    if (imageChanged) 
    {      
      var imageNumber = Utility.getRandomStringWithLength(6);
      ImageService.uploadImage($scope.signupData.imageData,imageNumber);
      //alert("image changed--"+imageNumber);  
    }   
    else
    {
      updateProfile();
    }  
  };

  $scope.$on('imageFile:uploaded', function(e, imageNumber)
  {    
    $scope.signupData.imageUrl="https://s3-ap-southeast-1.amazonaws.com/salonfiles/images/"+imageNumber+".jpeg";    
    //alert("DATA is---"+JSON.stringify($scope.signupData));
    updateProfile();    
  });

  var updateProfile=function()
  {
    $scope.signupData.deviceToken=SessionService.getDeviceToken();
    var url=API_URL+"users/";
    $http.put(url, $scope.signupData)
    .success(function(response)
    { 
      Utility.showToastMessage("Successfully updated.");                      
      $scope.loader=false;     
    })
    .error(function(response) 
    {       
      $scope.loader=false;
      Utility.showToastMessage("error..");
    });
  };

})

.controller('BuisnessCtrl', function($scope,$ionicModal,API_URL,$ionicLoading,$http,Utility,BROWSER_MODE)
{
  $scope.buisness={};
  $scope.$on('$ionicView.beforeEnter', function() 
  {
    $scope.loader=false;
  }); 
  
  $scope.submitBuisness=function()
  {
    if (!$scope.buisness.checked) 
    {
      Utility.showToastMessage("Please accept our terms and conditions...!");
      return;
    }
    if (!$scope.buisness.name) 
    {
      Utility.showToastMessage("Please Enter your name...!");
      return;
    }
    if (!$scope.buisness.mobile) 
    {
      Utility.showToastMessage("Please Enter your contact Number...!");
      return;
    }
    if (!$scope.buisness.address) 
    {
      Utility.showToastMessage("Please Enter address of your buisness...!");
      return;
    }
    var url=API_URL+"mails/buisness";
    $scope.loader=true;
    $http.post(url, $scope.buisness)
    .success(function(response)
    { 
      $scope.loader=false;
      $scope.buisness={};
      Utility.showToastMessage("Your information has been submitted...");
      //$state.go("app.main");
    })
    .error(function(response) 
    {       
      $scope.loader=false;
      Utility.showToastMessage("error..");
      console.log('error');
    }); 
  };

  $ionicModal.fromTemplateUrl('templates/vender-terms.html',
  {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function($ionicModal) 
  {
    $scope.venderModal = $ionicModal;
  });
})

.controller('ContactCtrl', function($scope,API_URL,$state,$timeout,$filter,BROWSER_MODE,$ionicLoading,$http,SessionService, $stateParams,Utility)
{
  $scope.contact={};
  $scope.$on('$ionicView.beforeEnter', function() 
  {
    $scope.loader=false;
  }); 

  $scope.submitContact=function()
  {
    if (!$scope.contact.name) 
    {
      Utility.showToastMessage("Please Enter your name...!");
      return;
    }
    if (!$scope.contact.subject) 
    {
      Utility.showToastMessage("Please Enter your subject...!");
      return;
    }
    if (!$scope.contact.text) 
    {
      Utility.showToastMessage("Please Enter your query...!");
      return;
    }
    if (!$scope.contact.email) 
    {
      Utility.showToastMessage("Please Enter your email...!");
      return;
    }
    var url=API_URL+"mails/contact";
    $scope.loader=true;
    $http.post(url, $scope.contact)
    .success(function(response)
    { 
      $scope.contact= {};  
      $scope.loader=false;
      Utility.showToastMessage("Your information has been submitted...");
      //$state.go("app.main");
    })
    .error(function(response) 
    {       
      $scope.loader=false;
      Utility.showToastMessage("error..");
      console.log('error');
    }); 
  };
})